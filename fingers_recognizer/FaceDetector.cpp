#include "FaceDetector.h"
#include <opencv2/opencv.hpp>

// Limit liczby wykrytch twarzy do 1 (alternatywa do removeFaces)
Rect getFaceRect(Mat input);

// Ścieżka do zapisanych parametrów klasyfikatora do wykrywania twarzy
String faceClassifierFileName = "/home/alex/code/C++/handy/res/haarcascade_frontalface_alt.xml";
// Klasyfikator
CascadeClassifier faceCascadeClassifier;

FaceDetector::FaceDetector(void) {
	// Rzuć wyjątek, jeśli nie wczytano pliku do klasyfikatora
	if (!faceCascadeClassifier.load(faceClassifierFileName))
		throw runtime_error("can't load file " + faceClassifierFileName);
}

void FaceDetector::removeFaces(Mat input, Mat output) {
	// tablica przechowująca wykryte twarze
	vector<Rect> faces;
	Mat frameGray;

	// convert BGR -> GRAY
	cvtColor(input, frameGray, COLOR_BGR2GRAY);
	// Zastosuj wyrównanie histogramu do powiększenia kontrastu na zdjęciu w odcieniach szarości
	equalizeHist(frameGray, frameGray);

	// Uruchom klasyfikator i wykryj twarze, po czym zwróć je jako obiekty klasy Rect do std::vector faces.
	// Ustaw minimalne wymiary(120x120) wykrywanych obiektów przez klasyfikator. Gdy obiekt będzie miał mniejsze zignoruj go.
	faceCascadeClassifier.detectMultiScale(frameGray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(120, 120));

	for (size_t i = 0; i < faces.size(); i++) {
		// Narysuj kwadrat wypełniony kolorem czarnym(-1) na miejscu wykrytych twarzy
		rectangle(
			output,
			Point(faces[i].x, faces[i].y),
			Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height),
			Scalar(0, 0, 0),
			-1
		);
	}
}

Rect getFaceRect(Mat input) {
	vector<Rect> faceRectangles;
	Mat inputGray;

	cvtColor(input, inputGray, COLOR_BGR2GRAY);
	equalizeHist(inputGray, inputGray);

	faceCascadeClassifier.detectMultiScale(inputGray, faceRectangles, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(120, 120));
	// Jeśli wykryto jakąkolwiek twarz, zwróć pierwszą wykrytą twarz
	if (faceRectangles.size() > 0)
		return faceRectangles[0];
	else
		return Rect(0, 0, 1, 1);
}