#pragma once

#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class FaceDetector {
	public:
		FaceDetector(void);
		// Narysuj kwadrat wypełniony kolorem czarnym na miejscu wykrytych twarzy
		void removeFaces(Mat input, Mat output);
};