#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

class FingerCount {
	public:
		FingerCount(void);
		// Do przeanalizowania
		Mat findFingersCount(Mat input_image, Mat frame);
	
	private:
		Scalar color_blue;
		Scalar color_green;
		Scalar color_red;
		Scalar color_black;
		Scalar color_white;
		Scalar color_yellow;
		Scalar color_purple;
		// Oblicz odległość między punktami (sqrt((a.x-b.x)^2 + (a.y-b.y)^2))
		double findPointsDistance(Point a, Point b);
		vector<Point> compactOnNeighborhoodMedian(vector<Point> points, double max_neighbor_distance);
		// Do przeanalizowania
		double findAngle(Point a, Point b, Point c);
		// Do przeanalizowania
		bool isFinger(Point a, Point b, Point c, double limit_angle_inf, double limit_angle_sup, cv::Point palm_center, double distance_from_palm_tollerance);
		// Do przeanalizowania
		vector<Point> findClosestOnX(vector<Point> points, Point pivot);
		// Oblicz odległość między punktami na osi x
		double findPointsDistanceOnX(Point a, Point b);
		// Rysowanie okręgów na końcówkach palców ... i nie tylko. Ewentualnie wstawianie tekstu oznaczającego ile palców zostało wyprostowanych
		void drawVectorPoints(Mat image, vector<Point> points, Scalar color, bool with_numbers);
};