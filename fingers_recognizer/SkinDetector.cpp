#include "SkinDetector.h"

SkinDetector::SkinDetector(void) {
	hLowThreshold = 0;
	hHighThreshold = 0;
	sLowThreshold = 0;
	sHighThreshold = 0;
	vLowThreshold = 0;
	vHighThreshold = 0;

	calibrated = false;

	skinColorSamplerRectangle1, skinColorSamplerRectangle2;
}

void SkinDetector::drawSkinColorSampler(Mat input) {
	int frameWidth = input.size().width, frameHeight = input.size().height;

	// rozmiar kwadratów
	int rectangleSize = 20;
	// kolor kwadratów
	Scalar rectangleColor = Scalar(255, 0, 255);

	// ustalenie pozycji kwadratu(dolny) w każdej klatce
	skinColorSamplerRectangle1 = Rect(frameWidth / 5, frameHeight / 2, rectangleSize, rectangleSize);
	// ustalenie pozycji kwadratu(górny) w kazdej klatce
	skinColorSamplerRectangle2 = Rect(frameWidth / 5, frameHeight / 3, rectangleSize, rectangleSize);

	// rysowanie kwadratu na klatce
	rectangle(
		input,
		skinColorSamplerRectangle1,
		rectangleColor
	);

	rectangle(
		input,
		skinColorSamplerRectangle2,
		rectangleColor
	);
}

void SkinDetector::calibrate(Mat input) {
	
	Mat hsvInput;
	// convert BGR -> HSV
	cvtColor(input, hsvInput, COLOR_BGR2HSV);

	// pobierz macierz reprezentującą dolny kwadrat z klatki HSV
	Mat sample1 = Mat(hsvInput, skinColorSamplerRectangle1);
	// pobierz macierz reprezentującą górny kwadrat z klatki HSV
	Mat sample2 = Mat(hsvInput, skinColorSamplerRectangle2);

	calculateThresholds(sample1, sample2);

	calibrated = true;
}

void SkinDetector::calculateThresholds(Mat sample1, Mat sample2) {
	// Magiczne liczby do obliczania wartości progu dla wychycenia koloru skóry z ręki
	int offsetLowThreshold = 80;
	int offsetHighThreshold = 30;

	// średnia wartość pikseli dla kwadratu dolnego na każdym kanale Scalar(H, S, V)
	Scalar hsvMeansSample1 = mean(sample1);
	// średnia wartość pikseli dla kwadratu górnego na każdym kanale Scalar(H, S, V)
	Scalar hsvMeansSample2 = mean(sample2);

	// Dolny próg i górny próg dla kanału H
	hLowThreshold = min(hsvMeansSample1[0], hsvMeansSample2[0]) - offsetLowThreshold;
	hHighThreshold = max(hsvMeansSample1[0], hsvMeansSample2[0]) + offsetHighThreshold;

	// Dolny próg i górny próg dla kanału S
	sLowThreshold = min(hsvMeansSample1[1], hsvMeansSample2[1]) - offsetLowThreshold;
	sHighThreshold = max(hsvMeansSample1[1], hsvMeansSample2[1]) + offsetHighThreshold;

	// Dolny próg i górny próg dla kanału V, (nie powinniśmy z niego korzystać) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// the V channel shouldn't be used. By ignorint it, shadows on the hand wouldn't interfire with segmentation.
	// Unfortunately there's a bug somewhere and not using the V channel causes some problem. This shouldn't be too hard to fix.
	vLowThreshold = min(hsvMeansSample1[2], hsvMeansSample2[2]) - offsetLowThreshold;
	vHighThreshold = max(hsvMeansSample1[2], hsvMeansSample2[2]) + offsetHighThreshold;
	//vLowThreshold = 0;
	//vHighThreshold = 255;
}

Mat SkinDetector::getSkinMask(Mat input) {
	Mat skinMask;

	// Jeśli nie określono progu, to zwróć macierz z zerami
	if (!calibrated) {
		skinMask = Mat::zeros(input.size(), CV_8UC1);
		return skinMask;
	}

	Mat hsvInput;
	// convert BGR -> HSV
	cvtColor(input, hsvInput, COLOR_BGR2HSV);

	// Wykryj skórę za pomocą podanego zakresu
	inRange(
		hsvInput,
		Scalar(hLowThreshold, sLowThreshold, vLowThreshold),
		Scalar(hHighThreshold, sHighThreshold, vHighThreshold),
		skinMask);

	// Przeprowadź "otwarcie", żeby zredukować szum w tle, false positivies, {3, 3} - kernel 3x3
	performOpening(skinMask, MORPH_ELLIPSE, { 3, 3 });
	// Po "otwarciu" przeprowadź dylację
	dilate(skinMask, skinMask, Mat(), Point(-1, -1), 3);

	return skinMask;
}

void SkinDetector::performOpening(Mat binaryImage, int kernelShape, Point kernelSize) {
	// getStructuringElement potrzebne do poprawnego wykonania morpohologyEx
	Mat structuringElement = getStructuringElement(kernelShape, kernelSize);
	// Operacja morfologiczna "otwarcia"
	morphologyEx(binaryImage, binaryImage, MORPH_OPEN, structuringElement);
}