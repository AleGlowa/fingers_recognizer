#include "BackgroundRemover.h"
#include <opencv2/opencv.hpp>

BackgroundRemover::BackgroundRemover(void) {
	background;
	calibrated = false;
}

void BackgroundRemover::calibrate(Mat input) {
	// convert BGR -> GRAY
	cvtColor(input, background, COLOR_BGR2GRAY);
	calibrated = true;
}

Mat BackgroundRemover::getForeground(Mat input) {
	Mat foregroundMask = getForegroundMask(input);

	//imshow("foregroundMask", foregroundMask);

	Mat foreground;
	// Kopiuj zawartość input Mat do foreground Mat z wybranymi pikselami przez maskę foregroundMask Mat
	input.copyTo(foreground, foregroundMask);

	return foreground;
}

Mat BackgroundRemover::getForegroundMask(Mat input) {
	Mat foregroundMask;

	// Jeśli nie przekonwertowano na odcienie szarości, to zwróć macierz z zerami
	if (!calibrated) {
		foregroundMask = Mat::zeros(input.size(), CV_8UC1);
		return foregroundMask;
	}

	// convert BGR -> GRAY
	cvtColor(input, foregroundMask, COLOR_BGR2GRAY);

	// maska i tło są w odcieniach szarości
	removeBackground(foregroundMask, background);
	
	return foregroundMask;
}

void BackgroundRemover::removeBackground(Mat input, Mat background) {
	// Magiczna liczba
	int thresholdOffset = 10;

	for (int i = 0; i < input.rows; i++) {
		for (int j = 0; j < input.cols; j++) {
			uchar framePixel = input.at<uchar>(i, j);
			uchar bgPixel = background.at<uchar>(i, j);

			if (framePixel >= bgPixel - thresholdOffset && framePixel <= bgPixel + thresholdOffset)
				// Ustaw czarny piskel na pozycji (i, j) w masce do wykrywania przedniego planu(foreground)
				input.at<uchar>(i, j) = 0;
			else
				// Ustaw biały piskel na pozycji (i, j) w masce do wykrywania przedniego planu(foreground)
				input.at<uchar>(i, j) = 255;
		}
	}
}