#pragma once

#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class SkinDetector {
	public:
		SkinDetector(void);

		// Rysuj kwadraty na klatce
		void drawSkinColorSampler(Mat input);
		// Określ wartości progu dla skóry z ręki
		void calibrate(Mat input);
		// Binaryzacja, zwróć klatke gdzie białe piksele oznaczają skórę
		Mat getSkinMask(Mat input);

	private:
		// Wartości progu dla skóry (dolna granica kanału H, górna granica kanału H, ...)
		int hLowThreshold = 0;
		int hHighThreshold = 0;
		int sLowThreshold = 0;
		int sHighThreshold = 0;
		int vLowThreshold = 0;
		int vHighThreshold = 0;

		// Określono próg?
		bool calibrated = false;

		// Kwadraty pobierające próbkę skóry
		Rect skinColorSamplerRectangle1, skinColorSamplerRectangle2;

		// Wylicz wartości progu dla skóry z ręki
		void calculateThresholds(Mat sample1, Mat sample2);
		// Operacja "otwarcia"
		void performOpening(Mat binaryImage, int structuralElementShapde, Point structuralElementSize);
};