#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>

#include "BackgroundRemover.h"
#include "SkinDetector.h"
#include "FaceDetector.h"
#include "FingerCount.h"

using namespace cv;
using namespace std;

int main(int, char**) {
	VideoCapture videoCapture(0);
	//videoCapture.set(CV_CAP_PROP_SETTINGS, 1);

	if (!videoCapture.isOpened()) {
		cout << "Can't find camera!" << endl;
		return -1;
	}

	// Ustaw rozmiar wychwytanego obrazu(HD)
    videoCapture.set(cv::CAP_PROP_FRAME_WIDTH, 1280);
    videoCapture.set(cv::CAP_PROP_FRAME_HEIGHT, 720);

	// frame - orginalna ramka, frameOut - skopiowana ramka, foreground - przedni plan(osoba)
	Mat frame, frameOut, handMask, foreground, fingerCountDebug;

	BackgroundRemover backgroundRemover;
	SkinDetector skinDetector;
	FaceDetector faceDetector;
	FingerCount fingerCount;

	while (true) {
		// pobieraj klatkę z kamery
		videoCapture >> frame;
		frameOut = frame.clone();

		// narysuj kwadraty(samples)
		skinDetector.drawSkinColorSampler(frameOut);
		// Usuń tło i zwróć przedni plan
		foreground = backgroundRemover.getForeground(frame);
		// Wstaw czarny kwadrat na miejsce twarzy
		faceDetector.removeFaces(frame, foreground);
		// Zwróć próbkę skóry na podstawie binarnego zdjęcia przedniego planu
		handMask = skinDetector.getSkinMask(foreground);
		fingerCountDebug = fingerCount.findFingersCount(handMask, frameOut);

		imshow("output", frameOut);
		imshow("foreground", foreground);
		imshow("handMask", handMask);
		imshow("handDetection", fingerCountDebug);
		
		int key = waitKey(1);

		if (key == 27) // esc
			break;
		else if (key == 98) // b
			backgroundRemover.calibrate(frame);
		else if (key == 115) // s
			skinDetector.calibrate(frame);
	}

	return 0;
}