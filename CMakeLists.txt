cmake_minimum_required(VERSION 2.8)
project(test_project)
find_package(OpenCV REQUIRED)

set(CMAKE_CXX_STANDARD 14)  # enable C++11 standard


set(SOURCES
${PROJECT_SOURCE_DIR}/Handy/main.cpp
${PROJECT_SOURCE_DIR}/Handy/SkinDetector.cpp
${PROJECT_SOURCE_DIR}/Handy/BackgroundRemover.cpp
${PROJECT_SOURCE_DIR}/Handy/FaceDetector.cpp
${PROJECT_SOURCE_DIR}/Handy/FingerCount.cpp
${PROJECT_SOURCE_DIR}/Handy/SkinDetector.h
${PROJECT_SOURCE_DIR}/Handy/BackgroundRemover.h
${PROJECT_SOURCE_DIR}/Handy/FaceDetector.h
${PROJECT_SOURCE_DIR}/Handy/FingerCount.h)

add_executable (handy ${SOURCES})

target_link_libraries(handy ${OpenCV_LIBS})