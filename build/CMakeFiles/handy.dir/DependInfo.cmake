# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/alex/code/C++/handy/Handy/BackgroundRemover.cpp" "/home/alex/code/C++/handy/build/CMakeFiles/handy.dir/Handy/BackgroundRemover.cpp.o"
  "/home/alex/code/C++/handy/Handy/FaceDetector.cpp" "/home/alex/code/C++/handy/build/CMakeFiles/handy.dir/Handy/FaceDetector.cpp.o"
  "/home/alex/code/C++/handy/Handy/FingerCount.cpp" "/home/alex/code/C++/handy/build/CMakeFiles/handy.dir/Handy/FingerCount.cpp.o"
  "/home/alex/code/C++/handy/Handy/SkinDetector.cpp" "/home/alex/code/C++/handy/build/CMakeFiles/handy.dir/Handy/SkinDetector.cpp.o"
  "/home/alex/code/C++/handy/Handy/main.cpp" "/home/alex/code/C++/handy/build/CMakeFiles/handy.dir/Handy/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
